<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
   'as' => 'api.',
], function () {
   Route::get('/carousel', [App\Http\Controllers\Api\SliderController::class, 'index']);
   Route::get('/about/{category}', [App\Http\Controllers\Api\AboutController::class, 'about']);
   Route::get('/divisi/{year}', [App\Http\Controllers\Api\StructureController::class, 'index']);
   Route::get('/member/{year}', [App\Http\Controllers\Api\MemberController::class, 'index']);
   Route::get('/member/{year}/{divisi}', [App\Http\Controllers\Api\MemberController::class, 'ondivisi']);
   Route::get('/berita', [App\Http\Controllers\Api\ContentController::class, 'index']);
   Route::get('/berita/{slug}', [App\Http\Controllers\Api\ContentController::class, 'show']);
   Route::get('/berita/kategori/{slug}', [App\Http\Controllers\Api\ContentController::class, 'indexByCategory']);
   Route::get('/category', [App\Http\Controllers\Api\ContentController::class, 'indexCategory']);
   Route::get('/kontak', [App\Http\Controllers\Api\ContactController::class, 'index']);
});
