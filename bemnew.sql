-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1,	'wawawdawd',	'wawawdawd',	'2021-02-02 06:44:59',	'2021-02-02 06:44:59'),
(2,	'Tentang Kami',	'tentang-kami',	'2021-02-02 06:47:08',	'2021-02-02 06:47:08'),
(3,	'Visi',	'visi',	'2021-02-02 06:47:14',	'2021-02-02 06:47:14'),
(4,	'Misi',	'misi',	'2021-02-02 06:47:19',	'2021-02-02 06:47:19');

DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contents_category_id_foreign` (`category_id`),
  CONSTRAINT `contents_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `contents` (`id`, `title`, `body`, `slug`, `status`, `category_id`, `created_at`, `updated_at`) VALUES
(1,	'Main di Bem FMIPA',	'<p style=\"text-align:justify;\">ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan. ini adalah konten untuk percobaan.&nbsp;</p>',	'main-di-bem-fmipa',	1,	1,	'2021-02-02 06:45:58',	'2021-02-02 06:45:58'),
(2,	'Hidup Bukan Hanya tentang Bernafas',	'<p style=\"text-align:justify;\">Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona Terpesona aku terpesona&nbsp;</p>',	'hidup-bukan-hanya-tentang-bernafas',	1,	1,	'2021-02-02 06:50:38',	'2021-02-02 06:50:38'),
(3,	'Tentang Kami',	'<p style=\"text-align:justify;\">Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami&nbsp;</p>',	'tentang-kami',	1,	2,	'2021-02-03 03:28:25',	'2021-02-03 03:28:25'),
(4,	'Visi',	'<p style=\"text-align:justify;\">Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami&nbsp;</p>',	'visi',	1,	3,	'2021-02-03 03:14:14',	'2021-02-03 03:14:14'),
(5,	'Misi',	'<ul style=\"list-style-type:circle;\"><li>Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami</li><li>Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami</li><li>Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami</li><li>Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami</li><li>Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami Kami Adalah Kami kami</li></ul>',	'misi',	1,	4,	'2021-02-03 03:22:28',	'2021-02-03 03:22:28'),
(6,	'efefef',	'<p>fefef</p>',	'efefef1612208882',	0,	1,	'2021-02-02 07:48:02',	'2021-02-02 07:48:02');

DROP TABLE IF EXISTS `content_tag`;
CREATE TABLE `content_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) unsigned NOT NULL,
  `tag_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_tag_content_id_foreign` (`content_id`),
  KEY `content_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `content_tag_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `content_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fileable_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileable_id` bigint(20) unsigned DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `files` (`id`, `fileable_type`, `fileable_id`, `link`, `type`, `created_at`, `updated_at`) VALUES
(1,	'App\\Models\\Content',	1,	'wawawdawd/Hc4De4.png',	'image',	'2021-02-02 06:45:52',	'2021-02-02 06:45:52'),
(4,	'App\\Models\\Content',	2,	'wawawdawd/mBuGNu.png',	'image',	'2021-02-02 06:50:32',	'2021-02-02 06:50:32'),
(12,	'App\\Models\\Content',	6,	'wawawdawd/V35Q1d.png',	'image',	'2021-02-02 07:48:08',	'2021-02-02 07:48:08'),
(14,	'App\\Models\\Structure',	1,	'structures/T882lR.png',	'image',	'2021-02-02 07:50:38',	'2021-02-02 07:50:38'),
(15,	'App\\Models\\Structure',	2,	'structures/tXazcL.png',	'image',	'2021-02-03 02:47:12',	'2021-02-03 02:47:12'),
(18,	'App\\Models\\Content',	4,	'visi/YvgxgN.png',	'image',	'2021-02-03 03:14:05',	'2021-02-03 03:14:05'),
(19,	'App\\Models\\Content',	5,	'misi/Cjl9gU.png',	'image',	'2021-02-03 03:15:28',	'2021-02-03 03:15:28'),
(20,	'App\\Models\\Content',	3,	'tentang kami/jzbg9l.png',	'image',	'2021-02-03 03:23:08',	'2021-02-03 03:23:08'),
(21,	'App\\Models\\Structure',	4,	'structures/GRmMNd.png',	'image',	'2021-02-03 03:26:59',	'2021-02-03 03:26:59'),
(24,	NULL,	NULL,	'slider/AIqCxN.png',	'slider',	'2021-02-12 19:09:29',	'2021-02-12 19:09:29'),
(26,	NULL,	NULL,	'slider/Jtul69.png',	'slider',	'2021-02-12 19:10:45',	'2021-02-12 19:10:45');

DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `structure_id` bigint(20) unsigned NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `members_structure_id_foreign` (`structure_id`),
  CONSTRAINT `members_structure_id_foreign` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2019_08_19_000000_create_failed_jobs_table',	1),
(4,	'2020_12_17_111726_create_posts_table',	1),
(5,	'2020_12_17_113716_create_categories_table',	1),
(6,	'2020_12_17_163454_create_tags_table',	1),
(7,	'2020_12_18_112731_create_contents_table',	1),
(8,	'2021_01_01_043503_create_content_tag_table',	1),
(9,	'2021_01_03_014502_create_files_table',	1),
(10,	'2021_01_29_023953_create_structures_table',	1),
(11,	'2021_01_30_023910_create_members_table',	1),
(12,	'2021_02_02_170410_create_timelines_table',	2);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `structures`;
CREATE TABLE `structures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `structures` (`id`, `name`, `description`, `year`, `created_at`, `updated_at`) VALUES
(1,	'Komunikasi dan Informasi',	'Kementrian ini ialah',	2020,	'2021-02-02 07:50:38',	'2021-02-02 07:50:38'),
(2,	'Advokesma',	'aku yang buat',	2020,	'2021-02-03 02:47:12',	'2021-02-03 02:47:12'),
(4,	'Presiden Mahasiswa',	'wefwef',	2020,	'2021-02-03 03:26:58',	'2021-02-03 03:26:58');

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `timelines`;
CREATE TABLE `timelines` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `structure_id` bigint(20) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `timelines_structure_id_foreign` (`structure_id`),
  CONSTRAINT `timelines_structure_id_foreign` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `timelines` (`id`, `name`, `date`, `structure_id`, `description`, `created_at`, `updated_at`) VALUES
(1,	'Bincang Bacot',	'24/02/2021',	1,	'bicang bersama dan bacot bersama',	'2021-02-03 02:48:13',	'2021-02-03 02:48:13'),
(3,	'macing mania',	'04/02/2021',	4,	'wefwef',	'2021-02-03 03:27:25',	'2021-02-03 03:27:25');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'irvan',	'admin@gmail.com',	'2021-02-02 06:40:20',	'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',	'YIZFnoyGEF',	'2021-02-02 06:40:20',	'2021-02-02 06:40:20');

-- 2021-03-25 14:18:11
