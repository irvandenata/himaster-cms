<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Content;
use App\Models\Structure;
use Illuminate\Http\Request;

class AboutController extends Controller
{
   public function about($category)
   {
      if ($category != "visi" & $category != "misi" & $category != "sejarah")
         return response()->json(
            "Not Found",
            404
         );
      $about = Category::where('slug', $category)->firstOrFail()->contents->firstOrFail();
      return response()->json(
         [
            "data" => $about,
            "status" => 201
         ]
      );
   }
}
